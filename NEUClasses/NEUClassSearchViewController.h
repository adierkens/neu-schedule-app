//
//  NEUClassSearchViewController.h
//  NEUClasses
//
//  Created by Adam Dierkens on 12/22/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NEUClassManager.h"

@interface NEUClassSearchViewController : UIViewController
<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *crnLabel;
@property (weak, nonatomic) IBOutlet UILabel *termLabel;
@property (weak, nonatomic) IBOutlet UILabel *instructorLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@property (weak, nonatomic) IBOutlet UILabel *buildingLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomLabel;
@property (weak, nonatomic) IBOutlet UILabel *capacityLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingLabel;
@property (weak, nonatomic) NEUClassManager* manager;
@end
