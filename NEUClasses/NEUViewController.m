//
//  NEUViewController.m
//  NEUClasses
//
//  Created by Adam Dierkens on 11/18/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import "NEUViewController.h"
#import "NEUClassManager.h"
#import "NEUClassDetailViewController.h"
#import "NEUClass.h"
#import "NEUClassSearchViewController.h"
#import "NEUScheduleViewController.h"

@interface NEUViewController ()
@property (strong, nonatomic) UIBarButtonItem *addButton;
@property (strong, nonatomic) UIBarButtonItem *refreshButton;
@property (strong, nonatomic) NSMutableArray* barButtonsArray;
@property (strong, nonatomic) UITableView* classTableView;
@property (strong, nonatomic) NEUClassManager *manager;
@property (strong, nonatomic) NEUClass* selectedClass;
@property (strong, nonatomic) NSDictionary* sortDict;
@property (strong, nonatomic) NSString* selectedSort;
@end

@implementation NEUViewController
@synthesize selectedSort;
# pragma mark - Initialize Variables
- (NEUClassManager*)manager{
    if (!_manager) _manager = [[NEUClassManager alloc] init];
    return _manager;
}
- (UIBarButtonItem*) addButton{
    if (!_addButton) _addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(_addPressed)];
    return _addButton;
}
-(NSMutableArray*) barButtonsArray{
    if (!_barButtonsArray) _barButtonsArray = [[NSMutableArray alloc] init];
    return _barButtonsArray;
}
-(UIBarButtonItem*) refreshButton{
    if (!_refreshButton) _refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(_refreshPressed)];
    return _refreshButton;
}
-(NSDictionary*)sortDict{
    if (!_sortDict) {
        NSMutableArray* keyarray = [NSMutableArray array];
        for (NSInteger i=0; i<6; i++){
            [keyarray addObject:[NSNumber numberWithInt:i]];
        }
        for (NSString* filter in [NSArray arrayWithObjects:@"CRN", @"Term", @"Instructor", @"Start Time", @"End Time", @"Days", @"Building", @"Room", @"Capacity", @"Remaining", @"Name", nil]) {
            [keyarray addObject:filter];
        }
        _sortDict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:@"Start Time", @"Days", @"Building", @"Capacity", @"Remaining", @"Name", @"crn", @"term", @"instructor", @"startTime", @"endTime", @"days", @"building", @"room", @"capacity", @"remaining", @"name", nil] forKeys:keyarray];
    }
    return _sortDict;
}
/*
 Sorting order:
    0. Start Time
    1. Days
    2. Building
    3. Capacity
    4. Remaining
    5. Name
    6. Cancel
 */


-(void)setFilterButtonText:(NSString*)text withColor:(UIColor *)color{
    UIBarButtonItem* filterbutton =  self.barButtonsArray[0];
    [filterbutton setTitle:text];
    [filterbutton setTitleTextAttributes:[NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName] forState:UIControlStateNormal];
}

# pragma mark - Setup button methods
-(void) _addPressed {
    NSLog(@"Add a class to the current view.");
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    UIAlertView* add = [[UIAlertView alloc] initWithTitle:@"Add a class" message:@"CRN Number" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
    add.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField* crn = [add textFieldAtIndex:0];
    crn.keyboardType = UIKeyboardTypeNumberPad;
    crn.placeholder = @"CRN Number";
    [add show];
}

-(void) _sortPressed {
        NSLog(@"Adding sort to view");
    UIActionSheet* filterActionSheet = [[UIActionSheet alloc] initWithTitle:@"Select filter" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Clear" otherButtonTitles:@"Start Time", @"Days", @"Building", @"Capacity", @"Remaining", @"Name", nil];
        [filterActionSheet showFromBarButtonItem:self.barButtonsArray[0] animated:YES];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        NSLog(@"Alert view Canceled");
    }else{
        NSString* text = [alertView textFieldAtIndex:0].text;

        if ([text length] == 4 || [text length] == 5){
            NSLog(@"Adding class to list. CRN: %@", [alertView textFieldAtIndex:0].text);
            [self.manager addClass:[alertView textFieldAtIndex:0].text];
            [self.tableView reloadData];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"Action Sheet");
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"]){
        NSLog(@"Cancel Selected");
        return;
    }else if (buttonIndex ==0){
        NSLog(@"Clear Sort");
        [self setSelectedSort:nil];
        UIBarButtonItem* sortButton =  self.barButtonsArray[0];
        [sortButton setTitle:@"Sort"];
    }else{
        NSString* selected = [self.sortDict objectForKey:[NSNumber numberWithInt:buttonIndex -1]];
        [self setSelectedSort:[self.sortDict objectForKey:selected]];
        NSLog(@"Selected Sort: %@", self.selectedSort);
        UIBarButtonItem* sortButton = self.barButtonsArray[0];
        [sortButton setTitle:[NSString stringWithFormat:@"Sorted: %@", selected]];
    }
    [self reload];
}


-(NSString *)getDBPath{
    NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:@"/neuclassdb"];
    return path;
}

-(void) _refreshPressed{
    NSLog(@"Refresh Pressed");
    [self.tableView reloadData];
}

-(void)reload{

    [self.tableView reloadData];
}


-(void)_searchPressed{
    NSLog(@"Pressed search button");
    NSLog(@"Loading search view");
    
    
    [self performSegueWithIdentifier:@"Schedule" sender:self];
    
}

# pragma mark - Setup View
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.manager getSQLite];
	self.navigationItem.leftBarButtonItem = self.refreshButton;
    UIBarButtonItem* searchButton = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStyleBordered target:self action:@selector(_searchPressed)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:self.addButton, searchButton , nil];
    [self.navigationController setToolbarHidden:NO];
    UIBarButtonItem* sortButton = [[UIBarButtonItem alloc] initWithTitle:@"Sort" style:UIBarButtonItemStylePlain target:self action:@selector(_sortPressed)];

    [self.barButtonsArray addObject:sortButton];
    [self setToolbarItems:self.barButtonsArray];
    self.classTableView = self.tableView;
    [self setTitle:@"Classes"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
# pragma mark - Table View
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.manager getClasses] count];
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"MainCell"];
    if (!cell) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainCell"];
    NSArray* classes = [[self.manager getClasses] copy];
    if (self.selectedSort){
        NEUClass* class = [[classes sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:self.selectedSort ascending:YES]]] objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%li - %@", (long)class.crn, class.name];
        return cell;
    }else{
    NEUClass* class = [classes objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%li - %@", (long)class.crn, class.name];
        return cell;
    }
}
- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete){
        [self.manager deleteClass:[[self.manager getClasses] objectAtIndex:indexPath.row]];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell* selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSString* crn = [selectedCell.textLabel.text componentsSeparatedByString:@" - "][0];
    [self setSelectedClass:[self.manager classWithCRN:crn]];
    [self performSegueWithIdentifier:@"ClassDetail" sender:self];
    }

# pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"Segueing to view: %@", [segue identifier]);
    if ([[segue identifier] isEqualToString:@"ClassDetail"]){
        NEUClassDetailViewController* nextView = [segue destinationViewController];
        nextView.cls = self.selectedClass;
    }else if ([[segue identifier] isEqualToString:@"search"]){
        NEUClassSearchViewController* nextView = [segue destinationViewController] ;
        [nextView setManager:self.manager];
    }else if ([[segue identifier] isEqualToString:@"Schedule"]) {
        NEUScheduleViewController* nextView = [segue destinationViewController];

        
        [nextView setClassArray:[self.manager getClasses]];
    }
}
@end
