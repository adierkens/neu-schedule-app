//
//  NEUClassSearch.h
//  NEUClasses
//
//  Created by Adam Dierkens on 1/23/14.
//  Copyright (c) 2014 Adam Dierkens. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NEUClass;
@interface NEUClassSearch : NSObject
+ (NSArray*)getClasses:(NSString *)term withInstructor:(NSString*) instructor withDays:(NSArray*)days withBuilding:(NSString*) building withRoom: (NSString*) room withCapacity:(NSString*)capacity withRemaining:(NSString*) remaining withCRN:(NSString*) crn;
+(NEUClass*)getClassWithCRN:(NSString *)crn;
@end
