//
//  NEUClass.m
//  NEUClasses
//
//  Created by Adam Dierkens on 12/7/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import "NEUClass.h"


@interface NEUClass()


@end


@implementation NEUClass
@synthesize crn, term, instructor, startTime, endTime, days, building, room, capacity, remaining, name;

+(BOOL)isclassAttribute:(NSString*)attr{
    NSArray* keyArray = [NSArray arrayWithObjects:@"crn", @"term", @"instructor", @"days", @"building", @"room", @"capacity", @"remaining", @"name", nil];
    if ([keyArray containsObject:attr]){
        return YES;
    }
    return NO;
}

-(BOOL)isComplete{
    
    if (!self.crn){
        NSLog(@"No crn");
        return NO;
    }else if (!self.term){
        NSLog(@"No term");
       // return NO;
    }else if (!self.instructor){
        NSLog(@"No instrunctor");
        return NO;
    }else if (!self.startTime){
        NSLog(@"No startTime");
        return NO;
    }else if (!self.endTime){
        NSLog(@"End Time");
        return NO;
    }else if (!self.days){
        NSLog(@"No days");
        return NO;
    }else if (!self.building){
        NSLog(@"No building");
        return NO;
    }else if (!self.room){
        NSLog(@"No Room");
        return NO;
    }else if (!self.capacity){
        NSLog(@"No capacity");
        return NO;
    }else if (!self.remaining){
        NSLog(@"No remaining");
        return NO;
    }else if (!self.name){
        NSLog(@"No name");
        return NO;
    }
    NSLog(@"CRN: %ld, Term: %@, Instr: %@, start: %@, end: %@, days: %@, building: %@, room: %ld, cap: %ld, remaining: %ld, name: %@", (long)self.crn, self.term, self.instructor, self.startTime, self.endTime, [self.days componentsJoinedByString:@","], self.building, (long)self.room, (long)self.capacity, (long)self.remaining, self.name);
    return YES;
}

+(NSString*) getClassAttrForKey:(NSString*)key{
    NSString* attr = [key lowercaseString];
    NSLog(@"Class Attr: %@", attr);
    if ([self isclassAttribute:attr]) {
        return attr;
    }else if ([attr isEqualToString:@"starttime"]){
        return @"startTime";
    }else if ([attr isEqualToString:@"endtime"]){
        return @"endTime";
    }
    
    return nil;
}


@end
