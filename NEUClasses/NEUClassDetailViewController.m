//
//  NEUClassDetailViewController.m
//  NEUClasses
//
//  Created by Adam Dierkens on 12/9/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import "NEUClassDetailViewController.h"
#import "NEUClass.h"

@interface NEUClassDetailViewController ()

@end

@implementation NEUClassDetailViewController
@synthesize crnLabel;
@synthesize termLabel;
@synthesize instructorLabel;
@synthesize startTimeLabel;
@synthesize endTimeLabel;
@synthesize daysLabel;
@synthesize buildingLabel;
@synthesize roomLabel;
@synthesize capacityLabel;
@synthesize remainingLabel;
@synthesize cls;
@synthesize longPressGesture;
NSString* outputFormat = @"Name - %@\nCRN - %d\nTerm - %@\nInstructor - %@\nStart Time - %@\nEnd Time - %@\nBuilding - %@\nRoom - %d\nCapacity - %d\nRemaining - %d";


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setTitle:self.cls.name];
    [self.crnLabel setText:[NSString stringWithFormat:@"%li", (long)self.cls.crn]];
    [self.termLabel setText:self.cls.term];
    [self.instructorLabel setText:self.cls.instructor];
    [self.startTimeLabel setText:self.cls.startTime];
    [self.endTimeLabel setText:self.cls.endTime];
    [self.daysLabel setText:[self.cls.days componentsJoinedByString:@", "]];
    [self.buildingLabel setText:self.cls.building];
    [self.roomLabel setText:[NSString stringWithFormat:@"%li", (long)self.cls.room]];
    [self.capacityLabel setText:[NSString stringWithFormat:@"%li", (long)self.cls.capacity]];
    [self.remainingLabel setText:[NSString stringWithFormat:@"%li", (long)self.cls.remaining]];
        
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
