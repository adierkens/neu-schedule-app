//
//  NEUScheduleViewController.h
//  NEUClasses
//
//  Created by Adam Dierkens on 2/1/14.
//  Copyright (c) 2014 Adam Dierkens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NEUScheduleViewController : UIViewController
@property (nonatomic, strong) NSArray* classArray;
@end
