//
//  NEUClassManager.h
//  NEUClasses
//
//  Created by Adam Dierkens on 11/22/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NEUClass;

@interface NEUClassManager : NSObject
-(void) getSQLite;
-(NSMutableArray*)getClasses;
-(NSArray*) getFilteredClasses;
-(void) deleteClass:(NEUClass *)cls;
-(void) addClass:(NSString *)crn;
-(void) addFilter:(NSString *)attribute value:(NSString *)value min:(NSInteger)min max:(NSInteger)max;
-(void) delFilter:(NSString *)attribute;
-(NSString *) getFilterString;
-(void)updateClassInfo:(NEUClass*)cls;
-(NEUClass*)classWithCRN:(NSString*)crn;
@property (strong, nonatomic) NSString* sort;
@end