//
//  NEUClassManager.m
//  NEUClasses
//
//  Created by Adam Dierkens on 11/22/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import "NEUClassManager.h"
#import "NEUClassSearch.h"
#import "NEUClass.h"
#include <sqlite3.h>
#include "TFHpple.h"


@interface NEUClassManager()
@property (nonatomic, strong) NSMutableArray* classes;
@property (nonatomic, strong) NSMutableDictionary* filters;
@end

@implementation NEUClassManager
@synthesize classes = _classes;
@synthesize filters = _filters;
@synthesize sort = _sort;
NSString * NEUBaseURL = @"https://wl11gp.neu.edu/udcprod8/NEUCLSS.p_class_search";

-(NSMutableArray*)classes{
    if (!_classes) _classes=[[NSMutableArray alloc] init];
    return _classes;
}

-(NSMutableDictionary*)filters{
    if (!_filters) _filters=[[NSMutableDictionary alloc] init];
    return _filters;
}

-(void) addClass:(NSString *)crn{
    NSLog(@"Adding class with CRN: %@", crn);
    for (NEUClass* cls in self.classes){
        if (cls.crn == [crn integerValue]){
            NSLog(@"Object with CRN already exists.");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Class with CRN already exists" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
    NEUClass* cls = [NEUClassSearch getClassWithCRN:crn];
    if (!cls){
        return;
    }
    [self.classes addObject:cls];
    [self writeObjectToDB:cls];
    
}

-(NSMutableArray*)getClasses{
    return self.classes;
}

-(void) addFilter:(NSString *)attribute value:(NSString *)value min:(NSInteger)min max:(NSInteger)max{
    if (value){
        [self.filters setValue:value forKey:attribute];
    }else if (min && max){
        NSArray* range = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:min], [NSNumber numberWithInt:max], nil];
        [self.filters setValue:range forKey:attribute];
    }
    
}

-(void)delFilter:(NSString *)attribute{
    if ([self.filters objectForKey:attribute]) {
        [self.filters removeObjectForKey:attribute];
    }
}

-(NSString*)getFilterString{
    NSString* filter = [[NSString alloc] init];
    
    
    return filter;
}


-(NSArray*)getFilteredClasses{
    NSMutableArray* filtered = [[NSMutableArray alloc] init];
    
    for (NEUClass* cls in self.classes){
        NSDictionary* targetDict = [cls dictionaryWithValuesForKeys:[NSArray arrayWithObject:[self.filters allKeys]]];
        for ( NSString* key in self.filters) {
            id filters = [self.filters objectForKey:key];
            if ([filters class] == [NSArray class]){
                // We have a range
                NSNumber* min = filters[0];
                NSNumber* max = filters[1];
                
                if ((NSInteger)[targetDict objectForKey:key] > [min integerValue] && (NSInteger)[targetDict objectForKey:key] < [max integerValue]){
                    continue;
                }else{
                    break;
                }
                
            }else if ([filters class] == [NSString class]){
                // Filter on specific string
                
                if ([(NSString*)[targetDict objectForKey:key] isEqualToString:(NSString*)filters]) {
                    continue;
                }else{
                    break;
                }
                
            }
            
            // We haven't broken out of the loop.
            // Class passed all filters
            
            [filtered addObject:cls];
            
            
            
        }
    
    }
    return filtered;
}

-(void)writeObjectToDB:(NEUClass*)cls{
    NSLog(@"Writing class to DB");
    [self createEditableCopyOfDatabaseIfNeeded];
    sqlite3* database;
    if (sqlite3_open([[self getDBPath] UTF8String], &database) == SQLITE_OK){
        NSLog(@"Opening SQLDB");
        NSString* stat = [ NSString stringWithFormat:@"INSERT INTO Classes(CRN, Name, Term, StartTime, EndTime, Instructor, Days, Building, Room, Capacity, Remaining) VALUES(%ld, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", %ld, %ld, %ld)", (long)cls.crn, cls.name, cls.term, cls.startTime, cls.endTime, cls.instructor, [cls.days componentsJoinedByString:@""], cls.building, (long)cls.room, (long)cls.capacity, (long)cls.remaining ];
        char* error;
        if (sqlite3_exec(database, [stat UTF8String], NULL, NULL, &error) == SQLITE_OK){
            NSLog(@"OK");
            
        }else{
            NSLog(@"INSERT => %@", stat);
            NSLog(@"ERROR => %@", [NSString stringWithUTF8String:error]);
        }
    }
}

-(void)deleteObjectToDB:(NEUClass*)cls{
    NSLog(@"Deleting class from DB");
    sqlite3* database;
    if (sqlite3_open([[self getDBPath] UTF8String], &database) == SQLITE_OK){
        NSLog(@"Delete SQL");
        NSString* stat = [ NSString stringWithFormat:@"DELETE FROM Classes WHERE CRN=%ld", (long)cls.crn];
        char* error;
        if (sqlite3_exec(database, [stat UTF8String], NULL, NULL, &error) == SQLITE_OK){
            NSLog(@"OK");
        }else{
            NSLog(@"DELETE => %@", stat);
            NSLog(@"ERROR => %@", [NSString stringWithUTF8String:error]);
        }
    }
}

-(void)getSQLite{
    NSLog(@"Getting SQLite DB");
    [self createEditableCopyOfDatabaseIfNeeded];
    sqlite3* database;
    if (sqlite3_open([[self getDBPath] UTF8String], &database) == SQLITE_OK){
        NSLog(@"Opening SQL DB");
        const char* sql = "select CRN, Term, StartTime, EndTime, Instructor, Days, Building, Room, Capacity, Remaining, Name FROM Classes";
        sqlite3_stmt* selectStatement;
        if(sqlite3_prepare_v2(database, sql, -1, &selectStatement, NULL) == SQLITE_OK){
            while(sqlite3_step(selectStatement) == SQLITE_ROW) {
                NSLog(@"Selecting Row");
                NEUClass* class = [[NEUClass alloc] init];
                class.crn = sqlite3_column_int(selectStatement, 0);
                class.term = [NSString stringWithUTF8String:sqlite3_column_text(selectStatement, 1)];
                class.startTime = [NSString stringWithUTF8String:sqlite3_column_text(selectStatement, 2)];
                class.endTime = [NSString stringWithUTF8String:sqlite3_column_text(selectStatement, 3)];
                class.instructor = [NSString stringWithUTF8String:sqlite3_column_text(selectStatement, 4)];
                class.days = [[NSString stringWithUTF8String:sqlite3_column_text(selectStatement, 5)] componentsSeparatedByString:@""];
                class.building = [NSString stringWithUTF8String:sqlite3_column_text(selectStatement, 6)];
                class.room = sqlite3_column_int(selectStatement, 7);
                class.capacity = sqlite3_column_int(selectStatement, 8);
                class.remaining = sqlite3_column_int(selectStatement, 9);
                class.name = [NSString stringWithUTF8String:sqlite3_column_text(selectStatement, 10)];
                [self.classes addObject:class];                
            }
        }
    }
    
}

-(NSData*)getData:(NSString *)crn{
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    NSString* post = [[NSString alloc] initWithFormat:@"sel_insm=None&sel_camp=None&begin_ap=a&sel_from_cred=None&sel_title=None&sel_instr=None&sel_attr=None&sel_subj=None&p_msg_code=UNSECURED&sel_to_cred=None&sel_crn=%@&end_ap=a&sel_schd=None&sel_crse=None&begin_mi=0&sel_seat=dummy&begin_hh=0&end_mi=0&sel_levl=None&end_hh=0&STU_TERM_IN=201430&sel_day=dummy&sel_ptrm=None", crn];
    NSURL *url=[NSURL URLWithString:NEUBaseURL];
    NSData* postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    NSError * error = nil;
    NSHTTPURLResponse *response = nil;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error){
        NSLog(@"Error: %@", error);
    }
    //NSLog(@"Data: %@", [[NSString alloc] initWithData:urlData encoding:NSASCIIStringEncoding]);
    return urlData;
}

-(NEUClass*)classWithCRN:(NSString*)crn{
    for (NEUClass* cls in self.classes) {
        if ( cls.crn == [crn integerValue] ){
            return cls;
        }
    }
    return nil;
}

-(void)updateClassInfo:(NEUClass*)cls{
    NEUClass* newClass = [NEUClassSearch getClassWithCRN:[NSString stringWithFormat:@"%ld", (long)cls.crn]];
    [self.classes removeObject:cls];
    [self.classes addObject:newClass];
    cls = nil;
}

-(NSString*)getDBPath{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* writablePath = [documentsDirectory stringByAppendingString:@"/neuclassdb"];
    return writablePath;
}

-(void)createEditableCopyOfDatabaseIfNeeded{
    //Test for existence
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSString* writablePath = [self getDBPath];
    success = [fileManager fileExistsAtPath:writablePath isDirectory:NO];
    if (success) return;
    NSString* defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"neuclassdb"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writablePath error:&error];
    if(!success){
        NSAssert1(0, @"Failed to create writable database path: %@", [error localizedDescription]);
    }
}

-(void)deleteDB{
    if([[NSFileManager defaultManager] fileExistsAtPath:[self getDBPath] isDirectory:NO]){
        [[NSFileManager defaultManager] removeItemAtPath:[self getDBPath] error:nil];
    }
}

-(void)deleteClass:(NEUClass *)cls{
    NSLog(@"Deleting class: %ld", (long)cls.crn);
    if([self.classes containsObject:cls]){
        [self.classes removeObject:cls];
    }
    [self deleteObjectToDB:cls];
}


@end
