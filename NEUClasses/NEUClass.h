//
//  NEUClass.h
//  NEUClasses
//
//  Created by Adam Dierkens on 12/7/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NEUClass : NSObject
@property (nonatomic, assign) NSInteger crn;
@property (nonatomic, retain) NSString* term;
@property (nonatomic, retain) NSString* instructor;
@property (nonatomic, retain) NSString* startTime;
@property (nonatomic, retain) NSString* endTime;
@property (nonatomic, retain) NSArray* days;
@property (nonatomic, retain) NSString* building;
@property (nonatomic, assign) NSInteger room;
@property (nonatomic, assign) NSInteger capacity;
@property (nonatomic, assign) NSInteger remaining;
@property (nonatomic, strong) NSString* name;
-(BOOL) isComplete;
+(NSString*) getClassAttrForKey:(NSString*)key;
@end
