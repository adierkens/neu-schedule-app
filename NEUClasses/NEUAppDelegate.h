//
//  NEUAppDelegate.h
//  NEUClasses
//
//  Created by Adam Dierkens on 11/18/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NEUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
