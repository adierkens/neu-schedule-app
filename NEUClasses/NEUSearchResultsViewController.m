//
//  NEUSearchResultsViewController.m
//  NEUClasses
//
//  Created by Adam Dierkens on 12/22/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import "NEUSearchResultsViewController.h"
#import "NEUClass.h"

@interface NEUSearchResultsViewController ()

@end

@implementation NEUSearchResultsViewController

@synthesize classArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [classArray count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NEUClass* class = [classArray objectAtIndex:indexPath.row];
    
    UICollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MainCell"  forIndexPath:indexPath];

    UITextView* text = [[UITextView alloc] initWithFrame:CGRectMake(5, 5, cell.frame.size.width - 10, cell.frame.size.height - 10)];
    [text setFont:[UIFont fontWithName:@"ArialMT" size:16]];
    [text setText:[NSString stringWithFormat:@"%ld\n" \
                                             @"%@\n" \
                                             @"%ld\n" \
                                             @"", (long)class.crn, class.term, (long)class.remaining]];
    [cell setBackgroundColor:[UIColor grayColor]];
    [cell addSubview:text];
    
    [UIView commitAnimations];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    NEUClass* selectedClass = [classArray objectAtIndex:indexPath.row];
    NSLog(@"Selected class with crn: %lu", (long)selectedClass.crn);
}
@end
