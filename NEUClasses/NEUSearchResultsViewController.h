//
//  NEUSearchResultsViewController.h
//  NEUClasses
//
//  Created by Adam Dierkens on 12/22/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NEUClass;
@interface NEUSearchResultsViewController : UICollectionViewController
<UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) NSArray* classArray;
@end
