//
//  main.m
//  NEUClasses
//
//  Created by Adam Dierkens on 11/18/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NEUAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NEUAppDelegate class]));
    }
}
