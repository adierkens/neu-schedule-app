//
//  NEUClassSearchViewController.m
//  NEUClasses
//
//  Created by Adam Dierkens on 12/22/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import "NEUClassSearchViewController.h"
#import "NEUSearchResultsViewController.h"
#import "NEUClass.h"
#import "NEUClassSearch.h"

/*
 Class Search Properties
 1. CRN - Decimal Pad Text Editor
 2. Term - Selection
 3. Instructor - Text Editor
 4. Start Time - Date-Time picker
 5. Days - Selection
 6. Building - Selection
 7. Room - Decimal Text Editor
 8. Capacity - Decimal text editor
 9. Remaining - Decimal text editor
 10. Name - Text Editor
 
 
*/

@interface NEUClassSearchViewController ()
@property (strong, nonatomic) NEUClass* searchClass;
@end

@implementation NEUClassSearchViewController
@synthesize searchClass = _searchClass;
@synthesize crnLabel = _crnLabel, termLabel = _termLabel, instructorLabel = _instructorLabel, startTimeLabel = _startTimeLabel, endTimeLabel = _endTimeLabel, daysLabel = _daysLabel, buildingLabel = _buildingLabel, roomLabel = _roomLabel, capacityLabel = _capacityLabel, remainingLabel = _remainingLabel;
@synthesize manager = _manager;

-(NEUClass*)searchClass{
    if (!_searchClass) _searchClass = [[NEUClass alloc] init];
    return _searchClass;
}

- (IBAction)didPressButton:(UIButton *)sender {
    NSLog(@"Button: %@ pressed", sender.titleLabel.text);
    [self displaySeachTextEditForAttr:sender.titleLabel.text];
}


-(void)displaySeachTextEditForAttr:(NSString*)attr{
    NSLog(@"Displaying text editor for cls attr: %@", attr);
    UIAlertView* addAttr = [[UIAlertView alloc] initWithTitle:attr message:@"Search Terms" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
    addAttr.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField* searchTerms = [addAttr textFieldAtIndex:0];
    if ([attr isEqualToString:@"CRN"] || [attr isEqualToString:@"Room"] || [attr isEqualToString:@"Capacity"] || [attr isEqualToString:@"Remaining"]) {
        // Set the keyboard Type to decimal
        [searchTerms setKeyboardType:UIKeyboardTypeNumberPad];
    }
    [addAttr show];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"]){
        return;
    }
    NSString* textInput = [alertView textFieldAtIndex:0].text;
    NSString* attr = [alertView title];
    NSString* classAttr = [NEUClass getClassAttrForKey:attr];
    if (!classAttr) {
        NSLog(@"ClassAttr is null");
    }else{
    [self.searchClass setValue:textInput forKey:[NEUClass getClassAttrForKey:attr]];
    [self refreshValues];
    }
}


-(void)refreshValues{
    self.crnLabel.text = [NSString stringWithFormat:@"%li", (long)[self.searchClass crn]];
    [self.termLabel setText:self.searchClass.term];
    [self.instructorLabel setText:self.searchClass.instructor];
    [self.startTimeLabel setText:self.searchClass.startTime];
    [self.endTimeLabel setText:self.searchClass.endTime];
    [self.daysLabel setText:[self.searchClass.days componentsJoinedByString:@", "]];
    [self.buildingLabel setText:self.searchClass.building];
    [self.roomLabel setText:[NSString stringWithFormat:@"%li", (long)self.searchClass.room]];
    [self.capacityLabel setText:[NSString stringWithFormat:@"%li", (long)self.searchClass.capacity]];
    [self.remainingLabel setText:[NSString stringWithFormat:@"%li", (long)self.searchClass.remaining]];
}


-(void)displaySearchSelectionforAttr:(NSString*)attr{
    NSLog(@"Displaying Selection for attr");
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)_search{
    NSLog(@"Searching with properties:");
    [self performSegueWithIdentifier:@"searchResults" sender:self];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIBarButtonItem* searchButton = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStyleBordered target:self action:@selector(_search)];
    [self.navigationItem setRightBarButtonItem:searchButton animated:YES];
    [self refreshValues];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"Segueing to view: %@", [segue identifier]);
    if ([[segue identifier] isEqualToString:@"searchResults"]) {
        NEUSearchResultsViewController* nextView = [segue destinationViewController];
       // [nextView setClassArray:[NEUClassSearch getClasses:[[self termLabel] text] withInstructor:[[self instructorLabel] text] withDays:[[[self daysLabel] text] componentsSeparatedByString:@","] withBuilding:[[self buildingLabel] text] withRoom:[[self roomLabel] text] withCapacity:[[self capacityLabel] text] withRemaining:[[self remainingLabel] text] withCRN: [[self crnLabel] text ]]];
        
        NSArray* testClass = [[NSArray alloc] initWithObjects:[[NEUClass alloc] init], nil];
        NEUClass* class = testClass[0];
        [class setCrn:32392];
        [class setName:@"Test Class"];
        [class setTerm:@"Test Term"];
        
        [nextView setClassArray:testClass];
    }
}

@end
