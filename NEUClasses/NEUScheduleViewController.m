//
//  NEUScheduleViewController.m
//  NEUClasses
//
//  Created by Adam Dierkens on 2/1/14.
//  Copyright (c) 2014 Adam Dierkens. All rights reserved.
//

#import "NEUScheduleViewController.h"
#import "NEUClass.h"

@interface NEUScheduleViewController ()

@end

@implementation NEUScheduleViewController
@synthesize classArray;


/*
 Build a graphical view of the weekly schedule
 
    M   T   W   R   F
 7
 8
 9
 10
 11
 12
 1
 2
 3
 4
 5
 6
 
 
 5 day week
    frame - 10? (time of day) / 5
    size of day in week
 
 12 Hours
    frame - 10? (day of week) / 12
    size of hour in day
 
 */


-(CGFloat)getTimeFromString:(NSString*) stringTime {
    NSArray *timeArr = [stringTime componentsSeparatedByString:@":"];
    NSInteger hour = [(NSString*)timeArr[0] integerValue];
    if (hour < 7 ) {
        hour += 12;
    }
    NSInteger min = [(NSString*)timeArr[1] integerValue];
    return hour/1.0 + min/60.0;
}

-(UIColor *)getColorFromCount:(NSInteger) count {
    if (count == 0) {
        return [UIColor redColor];
    } else if (count == 1) {
        return [UIColor blueColor];
    }
    
    return [UIColor grayColor];
}

-(NSArray*)createClassSubViews {
    NSLog(@"Creating subViews");
    NSMutableArray* subViews = [[NSMutableArray alloc] init];
    NSInteger totalHeight = self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height;
    NSInteger totalWidth = self.view.frame.size.width;
    NSInteger sizeOfHour = ((totalHeight - 10) / 12);
    
    NSLog(@"Height: %d", totalHeight);
    NSLog(@"Width: %d", totalWidth);
    
    for (NEUClass* class in classArray) {
        
        // Get the start and end times for the class
        // This will determine the Y value / height for the view
        CGFloat start, end;
        
        
        @try {
            start = [self getTimeFromString:class.startTime];
            end = [self getTimeFromString:class.endTime];
        }
        @catch (NSException * exc) {
            NSLog(@"Caught exception: %@", [exc description]);
            continue;
        }
        
        NSLog(@"Start: %f End: %f", start, end);
        
        NSInteger Yvalue = self.navigationController.navigationBar.frame.size.height + sizeOfHour * (start - 7);
        NSInteger height = sizeOfHour * (end - start);
        
        // For each day create a new view
        // This will determine the x values for the view
        // The width of the day is fixed
        // The days start at discrete values
        
        for (NSString* day in class.days) {
            NSInteger Xvalue;
            if ([day isEqualToString:@"M"]){
                Xvalue = 10;
            }else if ([day isEqualToString:@"T"]) {
                Xvalue = ((totalWidth - 10)/5);
            }else if ([day isEqualToString:@"W"]) {
                Xvalue = ((totalWidth - 10)/5) * 2;
            }else if ([day isEqualToString:@"R"]) {
                Xvalue = ((totalWidth - 10)/5) * 3;
            }else {
                Xvalue = ((totalWidth - 10)/5) * 4;
            }
            NSLog(@"Adding view with X: %d Y:%d Width: %d Height: %d", Xvalue, Yvalue, ((totalWidth - 10)/5), height);
            
            
            UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                            action:@selector(handleTap:)];
            
            UIView* sub = [[UIView alloc] initWithFrame:CGRectMake(Xvalue, Yvalue, ((totalWidth - 10)/5), height)];
            [sub setBackgroundColor:[self getColorFromCount:[classArray indexOfObject:class]]];
            [[sub layer] setBorderColor:[UIColor blackColor].CGColor];
            [[sub layer] setBorderWidth:2];
            [[sub layer] setCornerRadius:10];
            [sub addGestureRecognizer:tapRecognizer];
            [sub setTag:[classArray indexOfObject:class]];
            
            
            [subViews addObject:sub];
        }
    }
    
    NSLog(@"Returning %d views", [subViews count]);
    return subViews;
}

- (IBAction)handleTap:(UITapGestureRecognizer *)recognizer {
        if (recognizer.state == UIGestureRecognizerStateEnded){
            NEUClass* class = [classArray objectAtIndex:recognizer.view.tag];
            NSLog(@"Selected class: %@", class.name);
    }
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	NSArray* views = [self createClassSubViews];
    for (UIView* view in views) {
        [[self view] addSubview:view];
    }
    
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
