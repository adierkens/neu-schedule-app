//
//  NEUClassDetailViewController.h
//  NEUClasses
//
//  Created by Adam Dierkens on 12/9/13.
//  Copyright (c) 2013 Adam Dierkens. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NEUClass;
@interface NEUClassDetailViewController : UIViewController
@property (strong, nonatomic) NEUClass* cls;
@property (strong, nonatomic) IBOutlet UILabel *crnLabel;
@property (strong, nonatomic) IBOutlet UILabel *termLabel;
@property (strong, nonatomic) IBOutlet UILabel *instructorLabel;
@property (strong, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *daysLabel;
@property (strong, nonatomic) IBOutlet UILabel *buildingLabel;
@property (strong, nonatomic) IBOutlet UILabel *roomLabel;
@property (strong, nonatomic) IBOutlet UILabel *capacityLabel;
@property (strong, nonatomic) IBOutlet UILabel *remainingLabel;
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longPressGesture;

@end
