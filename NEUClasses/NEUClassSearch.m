//
//  NEUClassSearch.m
//  NEUClasses
//
//  Created by Adam Dierkens on 1/23/14.
//  Copyright (c) 2014 Adam Dierkens. All rights reserved.
//

#import "NEUClassSearch.h"
#import "NEUClass.h"
#import "TFHpple.h"


@interface NEUClassSearch()

@end

@implementation NEUClassSearch

/**
 Makes the query for the desired search.
 Checks for Errors in the HTTP request
 Returns the retrieved data
 */

+ (NSData*)makeQuery:(NSString *)term withInstructor:(NSString*) instructor withDays:(NSArray*)days withBuilding:(NSString*) building withRoom: (NSString*) room withCapacity:(NSString*)capacity withRemaining:(NSString*) remaining withCRN:(NSString*) crn {

    
    // Normalize data
    if (!term) term = @"201430";
    if (!instructor) instructor = @"%25";
    if (!building) building = @"";
    if (!room) room = @"";
    if (!capacity) capacity = @"";
    if (!remaining) remaining = @"";
    if (!crn) crn = @"";
    
    NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] init];
    NSString* emptyPostQuery = @"sel_subj=dummy&" \
                                "p_msg_code=UNSECURED&"\
                                "sel_attr=dummy&" \
                                "sel_seat=dummy&"\
                                "sel_schd=dummy&" \
                                "sel_camp=dummy&" \
                                "sel_insm=dummy&" \
                                "sel_instr=dummy&" \
                                "sel_levl=dummy&" \
                                "sel_day=dummy&" \
                                "sel_ptrm=dummy&" \
                                "begin_HH=0&" \
                                "begin_MI=0&" \
                                "begin_AP=a&" \
                                "end_HH=0&" \
                                "end_MI=0&" \
                                "end_AP=a&";
    
    NSString *postQueryFormat = @"STU_TERM_IN=%@&" \
                                "sel_attr=%@&" \
                                "sel_schd=%@&" \
                                "sel_camp=%@&" \
                                "sel_insm=%@&" \
                                "sel_instr=%@&" \
                                "sel_levl=%@&" \
                                "sel_ptrm=%@&" \
                                "sel_crn=%@&" \
                                "sel_subj=%@&" \
                                "sel_from_cred=%@&" \
                                "sel_to_cred=%@&" \
                                "sel_crse=%@&" \
                                "sel_title=%@";
    NSString* postString = [[NSString alloc] initWithFormat:postQueryFormat,
                            term, // STU_TERM_IN - 201430
                            @"%25", // sel_attr - %25
                            @"%25", // sel_schd - %25
                            @"%25", // sel_camp - %25
                            @"%25", // sel_insm - %25
                            instructor, // sel_instr - %25
                            @"%25", // sel_levl - %25
                            @"%25", // sel_prtm - %25
                            crn, /// sel_crn ''
                            @"%25", // sel_subj CS
                            @"",   // sel_from_cred - ''
                            @"",   // sel_to_cred - ''
                            @"",  // sel_crse - ''
                            @""]; // sel_title - ''
    
    postString = [emptyPostQuery stringByAppendingString:postString];
    
    NSURL* url = [NSURL URLWithString:@"https://wl11gp.neu.edu/udcprod8/NEUCLSS.p_class_search"];
    NSData* postData = [postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* postLength = [NSString stringWithFormat:@"%lu", (unsigned long) [postData length]];
    
    // Create the post Request
    [urlRequest setURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [urlRequest setHTTPBody:postData];
    
    NSError * error = nil;
    NSHTTPURLResponse * response = nil;
    
    // Get the response
    // TODO - make this async
    NSData * responseData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    // Check for errors
    if (error) {
        NSLog(@"Error sending class request: %@", error);
        return nil;
    }
  //  NSLog(@"Data: %@", [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding]);
    return responseData;
}


+(NEUClass*)getClassWithCRN:(NSString *)crn {
    NSArray* classArray = [self getClasses:nil withInstructor:nil withDays:nil withBuilding:nil withRoom:nil withCapacity:nil withRemaining:nil withCRN:crn];
    if ( [classArray count] != 1 ) {
        NSLog(@"Error getting class");
        return nil;
    }
    return classArray[0];
}


+ (NSArray*)getClasses:(NSString *)term withInstructor:(NSString*) instructor withDays:(NSArray*)days withBuilding:(NSString*) building withRoom: (NSString*) room withCapacity:(NSString*)capacity withRemaining:(NSString*) remaining withCRN:(NSString*) crn  {
    NSData* queryData = [self makeQuery:term withInstructor:instructor withDays:days withBuilding:building withRoom:room withCapacity:capacity withRemaining:remaining withCRN:crn];
    
    NSLog(@"Get Class Info Called");
    TFHpple* classParser = [TFHpple hppleWithHTMLData:queryData];
    int count = 0;
    NSString* queryString;
    NSArray* nodes;
    NSMutableArray* classesArray = [[NSMutableArray alloc] init];

    queryString = @"//th[@class='ddtitle']";
    nodes = [classParser searchWithXPathQuery:queryString];
    
    // The titles of all the classes returned in order
    // Create a new class obj for each
    for (TFHppleElement *element in nodes){
        
        [classesArray addObject:[[NEUClass alloc] init]];
        ((NEUClass*)classesArray.lastObject).name = [[[[element firstChild] firstChild] content] componentsSeparatedByString:@" - "][0];
        ((NEUClass*)classesArray.lastObject).crn = [[[[[element firstChild] firstChild] content] componentsSeparatedByString:@" - "][1] integerValue];
    }
    
    NSLog(@"Number of Classes returned: %lu", (unsigned long)[classesArray count]);
    
    // The classesArray now contains all the class objects
    // Fill in the rest of the info
    queryString = @"//td[@class='dddefault']";
    nodes = [classParser searchWithXPathQuery:queryString];

    // Every 10 elements begins the next class
    for (TFHppleElement *element in nodes){
        @try {
        if (count%10 == 0){
            TFHppleElement* t_ele = [element childrenWithTagName:@"text"][7];
            if ( [[[t_ele content] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 ){
                t_ele = [element childrenWithTagName:@"text"][10];
            }
            ((NEUClass*)classesArray[count/10]).instructor = [[t_ele content] componentsSeparatedByString:@" ("][0];
            
            t_ele = [element childrenWithTagName:@"text"][1];
            NSString* term =  [[t_ele content] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([term length] == 0) {
                term = [element childrenWithTagName:@"text"][4];
            }
            
            ((NEUClass*)classesArray[count/10]).term = [term stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            
        }else if (count%10 == 2) {
            NSArray* times =  [[[element firstChild] content] componentsSeparatedByString:@" - "];
            ((NEUClass*)classesArray[count/10]).startTime = times[0];
            ((NEUClass*)classesArray[count/10]).endTime = times[1];
            if (!((NEUClass*)classesArray[count/10]).startTime){
                ((NEUClass*)classesArray[count/10]).startTime = @"TBA";
            }
            if (!((NEUClass*)classesArray[count/10]).endTime){
                ((NEUClass*)classesArray[count/10]).endTime = @"TBA";
            }
        }else if (count%10 == 3){
            ((NEUClass*)classesArray[count/10]).days = [[[element firstChild] content] componentsSeparatedByString:@""];
        }else if (count == 4) {
            // Building and Room Num are together.
            NSString* tmp = [[element firstChild] content];
            NSArray* split = [tmp componentsSeparatedByString:@" "];
            ((NEUClass*)classesArray[count/10]).room = [[split lastObject] integerValue];
            ((NEUClass*)classesArray[count/10]).building = [[split subarrayWithRange:NSMakeRange(0, [split count] -1)] componentsJoinedByString:@" "];
        }else if (count%10 ==6){
            ((NEUClass*)classesArray[count/10]).remaining = [[[element firstChild] content] integerValue];
        }else if (count%10 == 9){
            ((NEUClass*)classesArray[count/10]).capacity = [[[element firstChild] content] integerValue];
        }
        }
        @catch(...){
            NSLog(@"Error getting class info, skipping to next class");
            while (count%10 != 9) {
                count++;
            }
                    }
        count++;
    }
    
    for (NEUClass* cls in classesArray){
            NSLog(@"CRN: %ld, Term: %@, Instr: %@, start: %@, end: %@, days: %@, building: %@, room: %ld, cap: %ld, remaining: %ld, name: %@",(long)cls.crn, cls.term, cls.instructor, cls.startTime, cls.endTime, [cls.days componentsJoinedByString:@","], cls.building, (long)cls.room, (long)cls.capacity, (long)cls.remaining, cls.name);
    }
    
    return classesArray;
}




@end
